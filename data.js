var APP_DATA = {
  "scenes": [
    {
      "id": "0-pintu-masuk",
      "name": "PINTU MASUK",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 2.398410575695433,
        "pitch": -0.019605518468303273,
        "fov": 1.4969451844739101
      },
      "linkHotspots": [
        {
          "yaw": 2.8947800806639368,
          "pitch": 0.04463938930402733,
          "rotation": 0,
          "target": "1-kasir"
        },
        {
          "yaw": 0.2735429631034716,
          "pitch": 0.16423853249622766,
          "rotation": 0,
          "target": "2-teknisi"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-kasir",
      "name": "KASIR",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 1.795322313383375,
        "pitch": 0.1829655880538752,
        "fov": 1.4969451844739101
      },
      "linkHotspots": [
        {
          "yaw": 1.2307201471521072,
          "pitch": 0.22254275426792702,
          "rotation": 0,
          "target": "0-pintu-masuk"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-teknisi",
      "name": "TEKNISI",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 1.2171217677797124,
        "pitch": 0.2208181067821755,
        "fov": 1.4969451844739101
      },
      "linkHotspots": [
        {
          "yaw": 1.4053528045416126,
          "pitch": 0.14923300102566905,
          "rotation": 0,
          "target": "0-pintu-masuk"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
